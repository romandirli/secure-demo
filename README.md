# Spring Security 6.0.0 Demo

## Scope of the application

* Users register as either client or contractor.
* Clients commission new tasks.
* Contractors accept existing tasks.


* For the sake of simplicity, many features (such as `PUT` and `DELETE`) are omitted.

## Security requirements

* Users hold accounts with their credentials (username and password).
* Users are authenticated using their credentials ([HTTP Basic](https://en.wikipedia.org/wiki/Basic_access_authentication)).
* Only authenticated users may access their respective account.
* Only clients commission tasks.
* Only contractors accept tasks.
* Users may be granted additional, elevated privileges.

---

## Implementation

This section solely discusses the contents of the package `isi.code.secure.security`.
The present inheritance hierarchy was simply chosen to fit role assignment easily.

```
   ,-----------.
   |Credentials|
   `-----------'
        1|
         |
         |1
     ,-------.
     |Account|
     `-------'
       ^   ^
      /     \
,------.   ,----------.
|Client|   |Contractor|
`------'   `----------'
```

### Role

In this context, the term user refers to the physical human being and by extension them _using_ the application.
`USER` is also a reserved keyword in SQL, the respective Hibernate entity is thus called `Account`.
Authenticated users are granted roles or authorities.

As a design choice, roles are modelled as an `enum` which expose their internal value both as a String as well as a `GrantedAuthority` object.

**NOTE:** When initially granted via `GrantedAuthority`, roles must be prefixed with `ROLE_`, whereas in the upcoming `SecurityConfiguration`, roles must not be prefixed with `ROLE_` as the `AuthorityAuthorizationManager` does this regardless.

### User details service

Spring provides in-memory and JDBC implementations of `UserDetailsService`.
For authentication backed by a CRUD or JPA repository, no default implementation is available.
Here, `JpaUserDetailsService` implements the interface `UserDetailsService`.
Given a user's name, the service produces

* their expected (salted and hashed) password
* a collection of authorities which may be granted to this user

via repository lookup.

This service makes use of the class `User`, Spring's default implementation of `UserDetails`.
In cases, where authentication via other credentials (for example, mail address and password) would be more appropriate, provide a suitable implementation for `UserDetails` as well.

### Password encoder

Passwords are salted and hashed before they are inserted into the database.
Spring's `BCryptPasswordEncoder` internally generates a random salt.

### Authentication provider

Each authentication provider performs a specific type of authentication.
The `DaoAuthenticationProvider` supports basic, username/password-based authentication.

This authentication provider relies on both the user details service and the password encoder.
When presented with a username and password, it encodes the password and compares the hash with the expected value provided by the user details lookup service.
If successful, the user is granted their eligible authorities.

**NOTE:** The `AuthenticationProvider` interface may be [deprecated soon](https://github.com/spring-projects/spring-security/issues/11428).

### Authentication facade

The currently authenticated user is available through `SecurityContextHolder.getContext().getAuthentication()`.

As a design choice, this static access is decoupled and hidden behind a facade.
Its respective implementation `AuthentificationFacadeImpl` exposes the authentication state as a Spring component.

### Security filter chain

The filter chain uses pattern matching to authorize requests.
The chain should transition from specific to more general rules.

```java
.antMatchers("/h2-console/**")   // Requests to this endpoint
.permitAll()                     // are always permitted.

.antMatchers("/api/task/**")     // Requests to this endpoint
.authenticated()                 // must be authenticated.

.antMatchers(
  POST,                          // Commissioning a new task
  "/api/account/task"            // via this endpoint
).hasRole(CLIENT.getRole())      // is for clients only.

.antMatchers(
  PUT,                           // Accepting a task
  "/api/account/task"            // via this endpoint
).hasRole(CONTRACTOR.getRole())  // is for contractors only.

.antMatchers("/api/account/**")  // Accessing their respective account
.authenticated()                 // is for either authenticated user.

.antMatchers(
  POST,                          // Creating a new account
  "/api/client",                 // via this endpoint
  "/api/contractor"              // or this endpoint
).permitAll()                    // is always permitted.

.antMatchers(                    // However,
  "/api/client/**",              // any other request to this endpoint
  "/api/contractor/**"           // or this endpoint
).hasRole(ADMIN.getRole())       // requires elevated privileges.

.anyRequest()                    // Requests not explicitly whitelisted above
.denyAll()                       // are denied.
```

Additionally, [CSRF](https://en.wikipedia.org/wiki/Cross-site_request_forgery) is disabled whereas HTTP Basic is enabled.
There is no need to create and maintain a session to persist the authentication across requests.
An authentication mechanism like HTTP Basic is [stateless](https://docs.spring.io/spring-security/reference/servlet/authentication/session-management.html#stateless-authentication) and, therefore, re-authenticates the user on every request.

### Security configuration

`PasswordEncoder`, `AuthenticationProvider` and `SecurityFilterChain` are all located as Spring beans in `@Configuration @EnableWebSecurity class SecurityConfiguration`.

---

## Communication with a frontend

In basic access authentication, the credentials (username and password) are joined by a single colon `:`, encoded into Base64 and prefixed with `Basic `.
HTTP requests then hold this value in the header field `Authorization`.

* Using _Axios_, provide credentials using `axios.get(url, { auth: { username: 'alice', password: '123' } })`.
* Using _Postman_, set the authorization type to _Basic Auth_.

This demo project also contains a collection of use cases in `requests.http`.