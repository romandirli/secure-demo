package isi.code.secure.service;

import isi.code.secure.model.Contractor;
import isi.code.secure.repository.ContractorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class ContractorService {
	final ContractorRepository contractors;
	final CredentialsService credentials;

	public Collection<Contractor> all() {
		return contractors.findAll();
	}

	public void save(String username, String password) {
		contractors.save(
			Contractor
				.builder()
				.credentials(credentials.save(username, password, false))
				.build()
		);
	}
}
