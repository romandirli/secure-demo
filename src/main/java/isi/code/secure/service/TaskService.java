package isi.code.secure.service;

import isi.code.secure.model.Client;
import isi.code.secure.model.Contractor;
import isi.code.secure.model.Task;
import isi.code.secure.repository.ClientRepository;
import isi.code.secure.repository.ContractorRepository;
import isi.code.secure.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class TaskService {
	final TaskRepository tasks;
	final ClientRepository clients;
	final ContractorRepository contractors;

	public Collection<Task> all() {
		return tasks.findAll();
	}

	public void save(String description, Client client) {
		client.getTasks().add(
			tasks.save(
				Task.builder()
					.description(description)
					.client(client)
					.build()
			)
		);
		clients.save(client);
	}

	public void save(Long id, Contractor contractor) {
		contractor.getTasks().add(tasks.getReferenceById(id));
		contractors.save(contractor);
	}
}
