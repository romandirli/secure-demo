package isi.code.secure.service;

import isi.code.secure.model.Account;
import isi.code.secure.model.Client;
import isi.code.secure.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class ClientService {
	final ClientRepository clients;
	final CredentialsService credentials;

	public Collection<Client> all() {
		return clients.findAll();
	}

	public Account byUsername(String username) {
		return clients.findByUsername(username);
	}

	public void save(String username, String password) {
		clients.save(
			Client
				.builder()
				.credentials(this.credentials.save(username, password, false))
				.build()
		);
	}
}
