package isi.code.secure.controller;

import isi.code.secure.model.Task;
import isi.code.secure.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping(path = "/api/task")
@RequiredArgsConstructor
public class TaskController {
	final TaskService tasks;

	@GetMapping
	Collection<Task> all() {
		return tasks.all();
	}
}
