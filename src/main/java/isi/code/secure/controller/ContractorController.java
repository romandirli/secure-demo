package isi.code.secure.controller;

import isi.code.secure.model.Contractor;
import isi.code.secure.service.ContractorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/contractor")
@RequiredArgsConstructor
public class ContractorController {
	final ContractorService contractors;

	@GetMapping
	Collection<Contractor> all() {
		return contractors.all();
	}

	@PostMapping
	void save(@RequestBody Map<String, String> payload) {
		contractors.save(payload.get("username"), payload.get("password"));
	}
}
