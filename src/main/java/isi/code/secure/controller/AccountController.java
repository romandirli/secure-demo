package isi.code.secure.controller;

import isi.code.secure.model.*;
import isi.code.secure.security.AuthenticationFacade;
import isi.code.secure.service.CredentialsService;
import isi.code.secure.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/account")
@RequiredArgsConstructor
public class AccountController {

	final CredentialsService credentials;
	final AuthenticationFacade authenticationFacade;
	final TaskService tasks;

	@GetMapping
	Credentials me() {
		return credentials.byUsername(authenticationFacade.getAuthentication().getName());
	}

	@GetMapping("/task")
	Collection<Task> all() {
		Account me = me().getAccount();
		if (me instanceof Client)
			return ((Client) me).getTasks();
		if (me instanceof Contractor)
			return ((Contractor) me).getTasks();
		return null;
	}

	@PostMapping("/task")
	void save(@RequestBody Map<String, String> payload) {
		tasks.save(payload.get("name"), (Client) me().getAccount());
	}

	@PutMapping("/task/{id}")
	void update(@PathVariable Long id) {
		tasks.save(id, (Contractor) me().getAccount());
	}
}
