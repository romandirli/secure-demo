package isi.code.secure.controller;

import isi.code.secure.model.Client;
import isi.code.secure.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/client")
@RequiredArgsConstructor
public class ClientController {
	final ClientService clients;

	@GetMapping
	Collection<Client> all() {
		return clients.all();
	}

	@PostMapping
	void save(@RequestBody Map<String, String> payload) {
		clients.save(payload.get("username"), payload.get("password"));
	}
}
