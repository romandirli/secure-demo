package isi.code.secure.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import java.util.Collection;

@Entity
@NoArgsConstructor @AllArgsConstructor @SuperBuilder @Getter
public class Client extends Account {
	@OneToMany
	Collection<Task> tasks;
}
