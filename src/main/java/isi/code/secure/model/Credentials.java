package isi.code.secure.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;

@Entity
@NoArgsConstructor @AllArgsConstructor @Builder @Getter
public class Credentials {
	@Id
	String username;

	String password;

	Boolean isAdmin;

	@JsonIgnore
	@OneToOne(mappedBy = "credentials")
	Account account;
}
