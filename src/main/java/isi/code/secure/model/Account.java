package isi.code.secure.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators.PropertyGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NoArgsConstructor @AllArgsConstructor @SuperBuilder @Getter
@JsonIdentityInfo(generator = PropertyGenerator.class, property = "username")
public abstract class Account {
	@MapsId
	@OneToOne
	protected Credentials credentials;

	@Id
	String username;
}
