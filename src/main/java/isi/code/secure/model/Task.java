package isi.code.secure.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators.PropertyGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;
import java.util.Collection;

@Entity
@NoArgsConstructor @AllArgsConstructor @Builder @Getter
@JsonIdentityInfo(generator = PropertyGenerator.class, property = "id")
public class Task {
	@Id
	@GeneratedValue
	Long id;

	String description;

	@JsonIgnoreProperties({"credentials", "tasks"})
	@OneToOne
	Client client;

	@JsonIgnoreProperties({"credentials", "tasks"})
	@ManyToMany(mappedBy = "tasks")
	Collection<Contractor> contractors;
}
