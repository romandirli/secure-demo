package isi.code.secure.repository;

import isi.code.secure.model.Client;
import isi.code.secure.model.Credentials;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Credentials> {
	Client findByUsername(String username);
}