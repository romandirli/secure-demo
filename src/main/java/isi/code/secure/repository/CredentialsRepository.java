package isi.code.secure.repository;

import isi.code.secure.model.Credentials;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CredentialsRepository extends JpaRepository<Credentials, String> {
	Credentials findByUsername(String username);
}