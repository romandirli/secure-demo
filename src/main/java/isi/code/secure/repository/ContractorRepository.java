package isi.code.secure.repository;

import isi.code.secure.model.Contractor;
import isi.code.secure.model.Credentials;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContractorRepository extends JpaRepository<Contractor, Credentials> {
	Contractor findByUsername(String username);
}