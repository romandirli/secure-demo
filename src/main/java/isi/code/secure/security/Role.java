package isi.code.secure.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@AllArgsConstructor @Getter
public enum Role {
	CLIENT("CLIENT"),
	CONTRACTOR("CONTRACTOR"),
	ADMIN("ADMIN");

	private final String role;

	public GrantedAuthority grant() {
		return new SimpleGrantedAuthority(role);
	}
}
