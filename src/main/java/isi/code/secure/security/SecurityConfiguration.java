package isi.code.secure.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static isi.code.secure.security.Role.*;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.security.config.Customizer.withDefaults;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	AuthenticationProvider authenticationProvider(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(userDetailsService);
		provider.setPasswordEncoder(passwordEncoder);
		return provider;
	}

	@Bean
	SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		return http
			.csrf().disable()
			.authorizeHttpRequests(authz -> authz
				.requestMatchers("/h2-console/**").permitAll()
				.requestMatchers("/api/task/**").authenticated()
				.requestMatchers(POST, "/api/account/task").hasAuthority(CLIENT.getRole())
				.requestMatchers(PUT, "/api/account/task").hasAuthority(CONTRACTOR.getRole())
				.requestMatchers("/api/account/**").authenticated()
				.requestMatchers(POST, "/api/client", "/api/contractor").permitAll()
				.requestMatchers("/api/client/**", "/api/contractor/**").hasAuthority(ADMIN.getRole())
				.anyRequest().denyAll()
			)
			.sessionManagement(session -> session.sessionCreationPolicy(STATELESS))
			.httpBasic(withDefaults())
			.build();
	}
}
