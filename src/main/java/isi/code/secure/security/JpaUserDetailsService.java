package isi.code.secure.security;

import isi.code.secure.model.Client;
import isi.code.secure.model.Contractor;
import isi.code.secure.model.Credentials;
import isi.code.secure.repository.CredentialsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static isi.code.secure.security.Role.*;

@Service @RequiredArgsConstructor
public class JpaUserDetailsService implements UserDetailsService {

	final CredentialsRepository credentials;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Credentials credentials = this.credentials.findByUsername(username);
		if (credentials != null) {
			List<GrantedAuthority> authorities = new ArrayList<>();
			if (credentials.getAccount() instanceof Client)
				authorities.add(CLIENT.grant());
			if (credentials.getAccount() instanceof Contractor)
				authorities.add(CONTRACTOR.grant());
			if (credentials.getIsAdmin())
				authorities.add(ADMIN.grant());
			return new User(credentials.getUsername(), credentials.getPassword(), authorities);
		}
		throw new UsernameNotFoundException(username);
	}
}
